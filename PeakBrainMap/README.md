Luca Chiarelli - Peak Brain Map Test


#Summary

The app has been developed keeping in mind the MVC pattern. The BrainMapViewController is in charge of retrieving the data from the model class and displaying them onto the view.

#Model

The model fetch its data from a property list file. This has been done in such way because:
1) I assumed the main purpouse of the exercise was to draw the brain map view
2) To allow the reviewer to easily modify the datasource from plist

There are 3 property list file in the project: 

- BrainMapConfiguration-6.plist
- BrainMapConfiguration-7.plist
- BrainMapConfiguration-8.plist

Each of them has different number of categories. Each category has a name and a color represented by a dictionary. By changing the name of the plist file in the model class, one can see also the BrainMapView mutating accordingly. 
The values of the polygons, which are also displayed in the category labels, are generated randomly and their number depends on the number of categories. So a polygon is always displayed with number of vertices as many as number of categories.

The app has been done in such way that, if desired, each of the plist file can be extended with n number of extra categories. The model and view will adapt accordingly.

There is also a property called animated which decided whether the polygons should scale up or not

#View Controller

The View Controller display the basic UI elements and initialise the creation of BrainMapView. It also provides (as specified in the specs) a UISegmentedControl, which is used to switch through the tabs and show different polygons.
The segmented control has 3 tabs:
- The first tab is for your personal values. Only one polygon is shown.
- The second tab is for age group values. A new polygon + the personal value polygon is shown
- The third tab is for professional values. A new polygon + the personal value polygon is shown

This process in parallel allows comparison between polygons. 

The values in the labels are always shown for the first polygon (white). But this could be changed if desired.

#View 

The View is scalable enough to support a flexible number of input data. The main drawing is done with UIBezierPath while the animation is done using CAShapeLayer.
The drawing of the grid and the polygons is indipendent and several methos in the public interface have been made avaiable for that. However all the drawing happens in DrawRect method, as supposed to.
The labels are drawed using rect and attributed string, even though also Core Text and CATextLAyer were considered.

#Memory

Run the app with Analyser --> OK
Run the app with Leak too  ---> No memory leaks found/Object allocation is stable

#Possible future improvements

- Adding animation for custom label values. There is no counter-up animation in the number displayed in the label. This couldn't be added mostly becuse of time constraints.
- Adding icons for sharing
- Implementing Unit Testing for key functionalities
- More refactoring of the BrainMapView, in particular for the method which is in charge of drawing the labels.

#Conclusions

In general all the requirements specified in the specs were met.

