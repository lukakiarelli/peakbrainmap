//
//  AppDelegate.h
//  PeakBrainMap
//
//  Created by Luca Chiarelli on 24/09/2014.
//  Copyright (c) 2014 lukakiarelli. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

