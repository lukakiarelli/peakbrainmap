//
//  PBMMainViewController.m
//  PeakBrainMap
//
//  Created by Luca Chiarelli on 24/09/2014.
//  Copyright (c) 2014 lukakiarelli. All rights reserved.
//

#import "PBMMainViewController.h"
#import "PBMBrainMapView.h"
#import "PBMColorUtils.h"
#import "PBMModel.h"

@interface PBMMainViewController ()

@property (nonatomic, strong) PBMModel *model;
@property (nonatomic, weak) IBOutlet PBMBrainMapView *brainMapView;
@property (nonatomic, weak) IBOutlet UILabel *titleLabel;
@property (nonatomic, weak) IBOutlet UIButton *shareButton;
@property (nonatomic, weak) IBOutlet UISegmentedControl *segmentedControl;

@property (nonatomic, strong) NSArray *personalValues;
@property (nonatomic, strong) NSArray *ageGroupValues;
@property (nonatomic, strong) NSArray *professionalValues;

@end

@implementation PBMMainViewController



- (instancetype)init
{
    self = [super init];
    if(self){
        _model = [PBMModel new];
        _personalValues = _model.randomValues;
        _professionalValues = _model.randomValues;
        _ageGroupValues = _model.randomValues;
    }
    return self;
}



- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    self.view.backgroundColor = [PBMColorUtils greyColor];
    // Navigation bar

    [self.navigationController.navigationBar setBackgroundImage:[UIImage new] forBarMetrics:UIBarMetricsDefault];
    [self.navigationController.navigationBar setShadowImage:[UIImage new]];
    self.navigationController.navigationBar.translucent = YES;
    
    self.navigationItem.title = @"Your Performance";
    
    NSString *titleText = @"Your Peak Brain Map";
    NSString *descriptionText = @"Review and compare how you're performing in each brain skill category";
    NSString *fullText = [NSString stringWithFormat:@"%@\n%@",[titleText uppercaseString],[descriptionText capitalizedString]];
    
    NSMutableAttributedString *attTitleText = [[NSMutableAttributedString alloc] initWithString: fullText];
    
    [attTitleText addAttribute: NSFontAttributeName
                      value: [UIFont fontWithName:@"Gotham-Medium" size:14.0]
                      range: NSMakeRange(0,[titleText length])];
    
    NSMutableParagraphStyle *style = [[NSMutableParagraphStyle alloc] init];
    [style setLineSpacing:4];
    [style setAlignment:NSTextAlignmentCenter];
    [attTitleText addAttribute:NSParagraphStyleAttributeName
                      value:style
                      range:NSMakeRange(0, [fullText length])];
    
    self.titleLabel.backgroundColor = [UIColor clearColor];
    self.titleLabel.numberOfLines = 1;
    self.titleLabel.textColor = [UIColor whiteColor];
    self.titleLabel.font = [UIFont fontWithName:@"Gotham-Light" size:14.0];
    self.titleLabel.backgroundColor = [UIColor clearColor];
    self.titleLabel.numberOfLines = 3;
    self.titleLabel.attributedText = attTitleText;
    
    NSString *shareText = @"Share";
    self.shareButton.titleLabel.font = [UIFont fontWithName:@"Gotham-Light" size:10.0];
    self.shareButton.backgroundColor = [UIColor clearColor];
    [self.shareButton setTitleColor:[UIColor lightGrayColor] forState:UIControlStateNormal];
    [self.shareButton setTitle:[shareText uppercaseString] forState:UIControlStateNormal];
    
    [self.segmentedControl addTarget:self
                              action:@selector(selectionChanged:)
                    forControlEvents:UIControlEventValueChanged];
    
    //get default categories
    //create the brain map object depending on the number of categories
    
    [self.brainMapView setAnimated:self.isAnimated];
    
    if(![self canDrawGrid]){
        self.model = nil;
        return;
    }
}


- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self.brainMapView updateWithCategories:self.model.categoryNames
                                     colors:self.model.categoryColors];
}



- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    [self showPersonalValues];
}



- (BOOL)isAnimated
{
    NSString *path = [[NSBundle mainBundle] pathForResource:MODEL_PLIST
                                                     ofType:@"plist"];
    NSDictionary *dict = [[NSDictionary alloc] initWithContentsOfFile:path];
    return [[dict objectForKey:@"Animated"] boolValue];
}



#pragma mark UISegmented Control

- (void)selectionChanged:(id)sender
{
    if([sender isKindOfClass:[UISegmentedControl class]])
    {
        UISegmentedControl *segmentedControl = (UISegmentedControl *)sender;
        NSInteger selectedIndex = [segmentedControl selectedSegmentIndex];
        if(selectedIndex == 0){
            [self showPersonalValues];
        }else if(selectedIndex == 1){
            [self showAgeGroupValues];
        } else {
            [self showProfessionalValues];
        }
    }
}



- (void)showPersonalValues{
    [self.brainMapView removePolygons];
    [self.brainMapView setPolygonValues:self.personalValues color:[UIColor whiteColor] displayValues:YES];
    [self.brainMapView drawPolygons];
}



- (void)showAgeGroupValues{
    [self.brainMapView removePolygons];
    [self.brainMapView setPolygonValues:self.personalValues color:[UIColor whiteColor] displayValues:NO];
    [self.brainMapView setPolygonValues:self.ageGroupValues color:[UIColor redColor] displayValues:YES];
    [self.brainMapView drawPolygons];
}



- (void)showProfessionalValues{
    [self.brainMapView removePolygons];
    [self.brainMapView setPolygonValues:self.personalValues color:[UIColor whiteColor] displayValues:NO];
    [self.brainMapView setPolygonValues:self.professionalValues color:[UIColor blueColor] displayValues:YES];
    [self.brainMapView drawPolygons];
}


#pragma mark error Handling


- (BOOL)canDrawGrid
{
    if([self.model.categoryNames count] != [self.model.categoryColors count]){
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Error", nil)
                                                        message:[NSString stringWithFormat:NSLocalizedString(@"There is a problem with your data - you must have same number of category names and colors - check your config file", nil), 1]
                                                       delegate:nil
                                              cancelButtonTitle:NSLocalizedString(@"OK", nil)
                                              otherButtonTitles:nil];
        [alert show];
        return NO;
    }
    if([self.model.categoryNames count] < 2){
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Error", nil)
                                                        message:[NSString stringWithFormat:NSLocalizedString(@"There is a problem with your data - Not enough categories were set - They should be at least two", nil), 1]
                                                       delegate:nil
                                              cancelButtonTitle:NSLocalizedString(@"OK", nil)
                                              otherButtonTitles:nil];
        [alert show];
        return NO;
    }
    
    return YES;
}




@end
