//
//  PBMColorUtils.h
//  PeakBrainMap
//
//  Created by Luca Chiarelli on 24/09/2014.
//  Copyright (c) 2014 lukakiarelli. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface PBMColorUtils : NSObject

+ (UIColor *)peakBlueColor;
+ (UIColor *)pinkColor;
+ (UIColor *)greenColor;
+ (UIColor *)purpleColor;
+ (UIColor *)orangeColor;
+ (UIColor *)blueColor;
+ (UIColor *)greyColor;
+ (UIColor *)lightGreyColor;
+ (UIColor *)redColor;
+ (UIColor *)colorFromTheme:(NSString *)theme;

@end
