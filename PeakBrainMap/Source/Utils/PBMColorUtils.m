//
//  PBMColorUtils.m
//  PeakBrainMap
//
//  Created by Luca Chiarelli on 24/09/2014.
//  Copyright (c) 2014 lukakiarelli. All rights reserved.
//

#import "PBMColorUtils.h"

@implementation PBMColorUtils


+ (UIColor *)peakBlueColor
{
    static UIColor *color = nil;
    if (color == nil) {
        color = [UIColor colorWithRed:0.0/255.0 green:184.0/255.0 blue:252.0/255.0 alpha:1.0];
    }
    return color;
}



+ (UIColor *)pinkColor
{
    static UIColor *color = nil;
    if (color == nil) {
        color = [UIColor colorWithRed:255.0/255.0 green:45.0/255.0 blue:85.0/255.0 alpha:1.0];
    }
    return color;
}



+ (UIColor *)greenColor
{
    static UIColor *color = nil;
    if (color == nil) {
        color = [UIColor colorWithRed:76.0/255.0 green:217.0/255.0 blue:100.0/255.0 alpha:1.0];
    }
    return color;
}



+ (UIColor *)purpleColor
{
    static UIColor *color = nil;
    if (color == nil) {
        color = [UIColor colorWithRed:88.0/255.0 green:86.0/255.0 blue:214.0/255.0 alpha:1.0];
    }
    return color;
}



+ (UIColor *)orangeColor
{
    static UIColor *color = nil;
    if (color == nil) {
        color = [UIColor colorWithRed:255.0/255.0 green:149.0/255.0 blue:0.0/255.0 alpha:1.0];
    }
    return color;
}



+ (UIColor *)blueColor
{
    static UIColor *color = nil;
    if (color == nil) {
        color = [UIColor colorWithRed:0.0/255.0 green:138.0/255.0 blue:255.0/255.0 alpha:1.0];
    }
    return color;
}



+ (UIColor *)greyColor
{
    static UIColor *color = nil;
    if (color == nil) {
        color = [UIColor colorWithRed:74.0/255.0 green:74.0/255.0 blue:74.0/255.0 alpha:1.0];
    }
    return color;
}



+ (UIColor *)lightGreyColor
{
    static UIColor *color = nil;
    if (color == nil) {
        color = [UIColor colorWithRed:142.0/255.0 green:142.0/255.0 blue:147.0/255.0 alpha:1.0];
    }
    return color;
}



+ (UIColor *)redColor
{
    static UIColor *color = nil;
    if (color == nil) {
        color = [UIColor colorWithRed:255.0/255.0 green:59.0/255.0 blue:48.0/255.0 alpha:1.0];
    }
    return color;
}



+ (UIColor *)colorFromTheme:(NSString *)theme
{
    if([theme isEqualToString:@"#red"]){
        return [PBMColorUtils redColor];
    }
    if([theme isEqualToString:@"#peakBlue"]){
        return [PBMColorUtils peakBlueColor];
    }
    if([theme isEqualToString:@"#blue"]){
        return [PBMColorUtils blueColor];
    }
    if([theme isEqualToString:@"#pink"]){
        return [PBMColorUtils pinkColor];
    }
    if([theme isEqualToString:@"#green"]){
        return [PBMColorUtils greenColor];
    }
    if([theme isEqualToString:@"#grey"]){
        return [PBMColorUtils greyColor];
    }
    if([theme isEqualToString:@"#lightGrey"]){
        return [PBMColorUtils lightGreyColor];
    }
    if([theme isEqualToString:@"#orange"]){
        return [PBMColorUtils orangeColor];
    }
    if([theme isEqualToString:@"#purple"]){
        return [PBMColorUtils purpleColor];
    }

    //default color
    return [UIColor blackColor];
}



@end
