//
//  PBMModel.h
//  PeakBrainMap
//
//  Created by Luca Chiarelli on 26/09/2014.
//  Copyright (c) 2014 lukakiarelli. All rights reserved.
//

#import <Foundation/Foundation.h>

extern NSString *const MODEL_PLIST;

@interface PBMModel : NSObject

@property (nonatomic, readonly) NSArray *categoryNames;
@property (nonatomic, readonly) NSArray *categoryColors;
@property (nonatomic, readonly) NSArray *randomValues;

@end
