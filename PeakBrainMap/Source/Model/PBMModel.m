//
//  PBMModel.m
//  PeakBrainMap
//
//  Created by Luca Chiarelli on 26/09/2014.
//  Copyright (c) 2014 lukakiarelli. All rights reserved.
//

#import "PBMModel.h"
#include "UIKit/UIKit.h"
#import "PBMColorUtils.h"


@implementation PBMModel

NSString *const MODEL_PLIST = @"BrainMapConfiguration-6";


- (NSArray *)categoryNames
{
    NSMutableArray *categoryNames = [NSMutableArray new];
    NSString *path = [[NSBundle mainBundle] pathForResource:MODEL_PLIST
                                                     ofType:@"plist"];
    NSDictionary *dict = [[NSDictionary alloc] initWithContentsOfFile:path];
    NSArray *categories = [dict objectForKey:@"Categories"];
    for(NSDictionary *category in categories){
        
        if([category valueForKey:@"Category"] == nil){
            break;
        }
        NSString *name = [category valueForKey:@"Category"];
        if(name != nil || ![name isEqualToString:@""]){
            [categoryNames addObject:name];
        }
    }

    return [NSArray arrayWithArray:categoryNames];
}



- (NSArray *)categoryColors
{
    NSMutableArray *categoryNames = [NSMutableArray new];
    NSString *path = [[NSBundle mainBundle] pathForResource:MODEL_PLIST
                                                     ofType:@"plist"];
    NSDictionary *dict = [[NSDictionary alloc] initWithContentsOfFile:path];
    NSArray *categories = [dict objectForKey:@"Categories"];
    for(NSDictionary *category in categories){
        
        if([category valueForKey:@"Color"] == nil){
            break;
        }
        NSString *colorName = [category valueForKey:@"Color"];
        UIColor *color = [PBMColorUtils colorFromTheme:colorName];
        if(color != nil){
            [categoryNames addObject:color];
        }
    }
    
    return [NSArray arrayWithArray:categoryNames];
}



- (NSArray *)randomValues
{
    NSInteger numberOfCategories = [[self categoryNames] count];
    NSMutableArray *randomValues = [NSMutableArray new];
    for (unsigned i = 0; i < numberOfCategories; i ++){
        NSInteger randomNumber = arc4random_uniform(1000);
        NSString *randomValue = [NSString stringWithFormat:@"%li",(long)randomNumber];
        [randomValues addObject:randomValue];
    }
    return [NSArray arrayWithArray:randomValues];
}


@end
