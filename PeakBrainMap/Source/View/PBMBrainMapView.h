//
//  PBMBrainMapView.h
//  PeakBrainMap
//
//  Created by Luca Chiarelli on 24/09/2014.
//  Copyright (c) 2014 lukakiarelli. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PBMBrainMapView : UIView

@property (nonatomic) BOOL animated;
@property (nonatomic) BOOL hasSecondPolygon;

- (void)updateWithCategories:(NSArray *)categories
                      colors:(NSArray *)colors;
- (void)update;
- (void)setCategories:(NSArray *)categories;
- (void)setColors:(NSArray *)colors;

- (void)setPolygonValues:(NSArray *)values
                   color:(UIColor *)color
            displayValues:(BOOL)shouldDisplayValues;

- (void)removePolygons;
- (void)drawPolygons;
- (void)drawPolygonWithValues:(NSArray *)values
                        color:(UIColor *)color
                displayValues:(BOOL)shouldDisplayValues;


@end
