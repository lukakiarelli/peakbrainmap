//
//  PBMBrainMapView.m
//  PeakBrainMap
//
//  Created by Luca Chiarelli on 24/09/2014.
//  Copyright (c) 2014 lukakiarelli. All rights reserved.
//

#include <math.h>
#import "PBMBrainMapView.h"
#import "PBMColorUtils.h"

#define DEGREES_TO_RADIANS(angle) ((angle) / 180.0 * M_PI)

@interface PBMBrainMapView()

@property (nonatomic, strong) NSArray *categories;
@property (nonatomic, strong) NSArray *colors;
@property (nonatomic, strong) NSMutableArray *polygonsValues;
@property (nonatomic, strong) NSArray *displayValues;
@property (nonatomic, strong) NSMutableArray *polygonsColors;
@property (nonatomic, strong) NSMutableArray *pathLayers;

@end

@implementation PBMBrainMapView


CGFloat const SATELLITE_CIRCLE_RADIUS = 5.5;
CGFloat const MAX_CIRCLE_RADIUS = 120.0;
CGFloat const EFFECTIVE_CIRCLE_RADIUS = 100.0;
CGFloat const TEXT_TO_GRID_DISTANCE = 10.0;
CGFloat const MAX_VALUE = 1000.0;
NSInteger const NUMBER_OF_CIRCLES = 10;



- (instancetype)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if(self){
        self.backgroundColor = [UIColor clearColor];
        _polygonsColors = [NSMutableArray new];
        _polygonsValues = [NSMutableArray new];
        _animated = NO;
    }
    return self;
}



- (void)updateWithCategories:(NSArray *)categories
                      colors:(NSArray *)colors
{
    _categories = categories;
    _colors = colors;
    [self setNeedsDisplay];
}



- (void)update
{
    _categories = [NSArray arrayWithObjects:@"category 1", @"category 2", nil];
    _colors = [NSArray arrayWithObjects:[UIColor whiteColor], [UIColor blackColor], nil];
    [self setNeedsDisplay];
}



- (void)setCategories:(NSArray *)categories
{
    _categories = categories;
}



- (void)setColors:(NSArray *)colors
{
    _colors = colors;
}



// display value bool governs the fact values for this polygon should be displayed or not on the view
- (void)setPolygonValues:(NSArray *)values
                   color:(UIColor *)color
            displayValues:(BOOL)shouldDisplayValues
{
    [self.polygonsColors addObject:color];
    [self.polygonsValues addObject:values];
    
    if(shouldDisplayValues){
        [self setDisplayValues:values];
    }
}



- (void)drawPolygons
{
    [self setNeedsDisplay];
}



- (void)drawPolygonWithValues:(NSArray *)values
                        color:(UIColor *)color
                displayValues:(BOOL)shouldDisplayValues
{
    [self setPolygonValues:values
                     color:color
             displayValues:shouldDisplayValues];
    [self setNeedsDisplay];
}



- (void)removePolygons
{
    self.layer.sublayers = nil;
    [self.polygonsValues removeAllObjects];
    [self.polygonsColors removeAllObjects];
    [self.pathLayers removeAllObjects];
    [self setNeedsDisplay];
}


// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
    
    if([self.categories count] < 2){
        NSLog(@"Not Enough Axis were set - you have to set minimum 2 axis");
        return;
    }
    
    if(([self.categories count] != [self.displayValues count]) && ([self.displayValues count] > 0)){
        NSLog(@"Invalid set of data - number of categories must match with number of values");
        return;
    }
    
    self.layer.sublayers = nil;
    
    //calculate distance from small satelite circles and center of the view ( = origin of the large circle)
    CGFloat outerCircleRadius = MAX_CIRCLE_RADIUS - SATELLITE_CIRCLE_RADIUS;
    CGPoint outerCircleOrigin = CGPointMake(self.bounds.size.width/2, self.bounds.size.height/2);
    
    for(unsigned i = 0; i < [self.categories count]; i ++){
        //get the values necessary for labels and rest of satellite circles
        CGFloat angle = [[[self angles]objectAtIndex:i] floatValue];
        UIColor *color = [self.colors objectAtIndex:i];
        NSString *category = [self.categories objectAtIndex:i];
        
        CGPoint satelliteCircleOrigin = [self pointForCircleWithRadius:outerCircleRadius
                                                                 angle:angle
                                                                origin:outerCircleOrigin];
        
        [self drawSatelliteCircleWithOrigin:satelliteCircleOrigin angle:angle color:color];
        if(self.displayValues){
            [self drawTextAtPoint:satelliteCircleOrigin
                       headerText:category
                       contentText:[self.displayValues objectAtIndex:i]];
        } else {
            // 0 is the default value which is set if no display values are passed
            [self drawTextAtPoint:satelliteCircleOrigin
                       headerText:category
                      contentText:@"0"];
        }
    }
    //draw concentric circles within the large circle
    [self drawInnerCircles];
    
    // retrieve all the polygons value and draw the polygons using UIBezierPath and then assigning it to shape layer for animation
    if([self.polygonsValues count] > 0 && [self.polygonsColors count] > 0 && [self.polygonsColors count] == [self.polygonsValues count]){
        
        for(unsigned i = 0; i < [self.polygonsValues count]; i ++){
            NSArray *polygonValues = [self.polygonsValues objectAtIndex:i];
            UIColor *polygonColor = [self.polygonsColors objectAtIndex:i];
            
            UIBezierPath *path = [self polygonPathWithValues:polygonValues];
            [self drawPolygonAtPath:path color:polygonColor animated:self.animated];
        }
    } else {
        [self.pathLayers removeAllObjects];
    }
}



#pragma mark map setting



- (void)drawSatelliteCircleWithOrigin:(CGPoint)origin
                                angle:(CGFloat)angle
                                color:(UIColor *)color
{
    //draw the coloured circle
    UIBezierPath *circlePath = [UIBezierPath bezierPath];
    [circlePath addArcWithCenter:origin radius:SATELLITE_CIRCLE_RADIUS startAngle:DEGREES_TO_RADIANS(angle - 180.0) endAngle:DEGREES_TO_RADIANS(angle + 180.0) clockwise:YES];
    [color setStroke];
    [circlePath stroke];
    [circlePath strokeWithBlendMode:kCGBlendModeNormal alpha:1.0];
    
    //draw the line which join with origin
    UIBezierPath *linePath = [UIBezierPath bezierPath];
    CGPoint point = [self pointForCircleWithRadius:SATELLITE_CIRCLE_RADIUS angle:angle + 180.0 origin:origin];
    [linePath moveToPoint:point];
    [linePath addLineToPoint:CGPointMake(self.bounds.size.width/2, self.bounds.size.height/2)];
    [[PBMColorUtils lightGreyColor] setStroke];
    [linePath stroke];
}



- (void)drawInnerCircles
{
    CGFloat widthCircles = EFFECTIVE_CIRCLE_RADIUS/NUMBER_OF_CIRCLES;
    int multiplier = 1;
    while (multiplier <= NUMBER_OF_CIRCLES) {
        CGFloat radius = (multiplier * widthCircles);
        [self drawInnerCircleForRadius:radius];
        multiplier +=1;
    }
}



- (void)drawInnerCircleForRadius:(CGFloat)circleRadius
{
    CGPoint centerView = CGPointMake(self.bounds.size.width/2, self.bounds.size.height/2);
    //draw the coloured circle
    UIBezierPath *circlePath = [UIBezierPath bezierPath];
    [circlePath addArcWithCenter:centerView radius:circleRadius startAngle:DEGREES_TO_RADIANS(0.0) endAngle:DEGREES_TO_RADIANS(360.0) clockwise:YES];
    [[PBMColorUtils lightGreyColor] setStroke];
    [circlePath stroke];
    [circlePath strokeWithBlendMode:kCGBlendModeNormal alpha:1.0];
}



//draw text with attributed string
- (void)drawTextAtPoint:(CGPoint)point
             headerText:(NSString *)headerText
            contentText:(NSString *)contentText;
{
    
    //default Values width
    CGFloat labelWidth = 150.0;
    
    //adjust the width accroding to position of the label
    if(point.x - labelWidth/2 < self.frame.origin.x){
        labelWidth = labelWidth/2;
    }
    
    if(point.x + labelWidth/2 > self.frame.size.width){
        labelWidth = labelWidth/2;
    }
    
    NSMutableParagraphStyle *paragraphStyle = [[NSParagraphStyle defaultParagraphStyle] mutableCopy];
    [paragraphStyle setLineSpacing:1];
    paragraphStyle.alignment = NSTextAlignmentCenter;
    
    //determine wether the label is above or belove the origin of the circle grid
    if(point.y < self.bounds.size.height/2){
        
        NSString *fullText = [NSString stringWithFormat:@"%@\n%@",headerText,contentText];
        NSMutableAttributedString *attString = [[NSMutableAttributedString alloc] initWithString: fullText];
        
        
        [attString addAttribute: NSFontAttributeName
                          value: [UIFont fontWithName:@"Gotham-Medium" size:14.0]
                          range: NSMakeRange(0,[fullText length])];
        
        [attString addAttribute: NSFontAttributeName
                          value: [UIFont fontWithName:@"Gotham-Light" size:14.0]
                          range: NSMakeRange(0,[headerText length])];
        [attString addAttribute: NSForegroundColorAttributeName value:[UIColor whiteColor] range:NSMakeRange(0,[fullText length])];
        
        
        [attString addAttribute:NSParagraphStyleAttributeName value:paragraphStyle range:NSMakeRange(0,[fullText length])];
        
        CGRect rect = [attString boundingRectWithSize:CGSizeMake(labelWidth, 10000) options:NSStringDrawingUsesLineFragmentOrigin | NSStringDrawingUsesFontLeading context:nil];
        
        rect.origin.x = point.x - rect.size.width/2;
        rect.origin.y = point.y - SATELLITE_CIRCLE_RADIUS - TEXT_TO_GRID_DISTANCE - rect.size.height;
        
        [attString drawInRect:rect];
        
    }else{
        
        NSString *fullText = [NSString stringWithFormat:@"%@\n%@",contentText,headerText];
        NSMutableAttributedString *attString = [[NSMutableAttributedString alloc] initWithString: fullText];
        
        [attString addAttribute: NSFontAttributeName
                          value: [UIFont fontWithName:@"Gotham-Light" size:14.0]
                          range: NSMakeRange(0,[fullText length])];
        [attString addAttribute: NSFontAttributeName
                          value: [UIFont fontWithName:@"Gotham-Medium" size:14.0]
                          range: NSMakeRange(0,[contentText length])];
        
        [attString addAttribute: NSForegroundColorAttributeName value:[UIColor whiteColor] range:NSMakeRange(0,[fullText length])];
        
        
        [attString addAttribute:NSParagraphStyleAttributeName value:paragraphStyle range:NSMakeRange(0,[fullText length])];
        
        CGRect rect = [attString boundingRectWithSize:CGSizeMake(labelWidth, 10000) options:NSStringDrawingUsesLineFragmentOrigin | NSStringDrawingUsesFontLeading context:nil];
        
        rect.origin.x = point.x - rect.size.width/2;
        rect.origin.y = point.y + SATELLITE_CIRCLE_RADIUS + TEXT_TO_GRID_DISTANCE;
        
        [attString drawInRect:rect];
    }
}



#pragma mark polygon


- (UIBezierPath *)polygonPathWithValues:(NSArray *)values
{

    UIBezierPath *polygonPath = [UIBezierPath bezierPath];
    CGFloat divider = MAX_VALUE/EFFECTIVE_CIRCLE_RADIUS;
    
    CGFloat radius = [[values objectAtIndex:0] floatValue]/divider;
    CGFloat angle = [[[self angles] objectAtIndex:0] floatValue];
    CGPoint origin = CGPointMake(self.bounds.size.width/2, self.bounds.size.height/2);
    
    CGPoint startPoint = [self pointForCircleWithRadius:radius
                                                  angle:angle
                                                 origin:origin];

    [polygonPath moveToPoint:startPoint];
    //get all the vertices of the polygon and it's angles respect to the vectors
    for(unsigned i = 0; i < [values count]; i ++){
        
        radius = [[values objectAtIndex:i] floatValue]/divider;
        angle = [[[self angles] objectAtIndex:i] floatValue];
        CGPoint currentPoint =  [self pointForCircleWithRadius:radius
                                                         angle:angle
                                                        origin:origin];
        [polygonPath addLineToPoint:currentPoint];
    }
    
    [polygonPath closePath];
    return polygonPath;
}



- (void)drawPolygonAtPath:(UIBezierPath *)path color:(UIColor *)color animated:(BOOL)animated
{
    //adjust center of polygon which otherwise would gravitate toward the center of the view during the animation
    CAShapeLayer *pathLayer = [CAShapeLayer layer];
    pathLayer.path = path.CGPath;
    CGRect polygonBoundingBox = CGPathGetBoundingBox(path.CGPath);
    pathLayer.bounds = polygonBoundingBox;
    
    // get offset between path layer and bound and adjust shape layer animation
    CGFloat offsetX = CGRectGetMidX(self.bounds) - CGRectGetMidX(pathLayer.bounds);
    CGFloat offsetY = CGRectGetMidY(self.bounds) - CGRectGetMidY(pathLayer.bounds);
    
    pathLayer.position = CGPointMake(self.bounds.size.width/2 - offsetX, self.bounds.size.height/2 - offsetY);
    
    UIColor *fillColor = color;
    [pathLayer setFillColor:[fillColor colorWithAlphaComponent:0.5].CGColor];
    [self.layer addSublayer:pathLayer];
    
    if(animated){
        CABasicAnimation *animation = [CABasicAnimation animationWithKeyPath:@"transform.scale"];
        [animation setFromValue:[NSNumber numberWithFloat:0.1f]];
        [animation setToValue:[NSNumber numberWithFloat:1.0f]];
        [animation setRemovedOnCompletion:NO];
        [animation setDuration:0.5];
        [animation setFillMode:kCAFillModeForwards];
        
        [pathLayer addAnimation:animation forKey:animation.keyPath];
    }
    [self.pathLayers addObject:pathLayer];
}



#pragma mark helpers


- (CGPoint)pointForCircleWithRadius:(CGFloat)radius
                             angle:(CGFloat)angleInDegrees
                            origin:(CGPoint)origin
{
    // Convert from degrees to radians via multiplication by PI/180
    float x = (float)(radius * cos(angleInDegrees * M_PI / 180.0)) + origin.x;
    float y = (float)(radius * sin(angleInDegrees * M_PI/ 180.0)) + origin.y;
    
    return CGPointMake(x, y);
}



- (NSArray *)angles
{
    //caslculate the first angle
    float angle = 180.0/[self.categories count];
    //calculate what is the angle for every section-interval
    float genericAngle = 360.0/[self.categories count];
    //set a starting point
    float degree = angle;
    
    NSMutableArray *angles = [NSMutableArray new];
    
    while (degree <= 360.0){
        //populate the array
        NSNumber *element =  [NSNumber numberWithFloat:degree];
        [angles addObject:element];
        degree += genericAngle;
    }
    return [NSArray arrayWithArray:angles];
}



@end
